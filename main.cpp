#include <QCoreApplication>

#include <optional>

#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusReply>
#include <QDBusUnixFileDescriptor>
#include <QDebug>

#include <QObject>

class Helper : public QObject
{
    Q_OBJECT

public:
    std::optional<QDBusUnixFileDescriptor> m_inh;
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    a.setApplicationName("call-testing");

    Helper helper;

    QDBusMessage inhibitCall = QDBusMessage::createMethodCall(QStringLiteral("org.freedesktop.login1"),
                                                              QStringLiteral("/org/freedesktop/login1"),
                                                              QStringLiteral("org.freedesktop.login1.Manager"),
                                                              QStringLiteral("Inhibit"));
    inhibitCall.setArguments({
                                 QStringLiteral("sleep"),
                                 QCoreApplication::instance()->applicationName(),
                                 QObject::tr("Active call inhibits system suspend"),
                                 QStringLiteral("block")
                             });
    QDBusReply<QDBusUnixFileDescriptor> reply = QDBusConnection::systemBus().call(inhibitCall);

    if (!reply.isValid()) {
        qDebug() << "logind sleep inhibitor: error: call failed";
    }

    auto const fd = reply.value();
    if (!fd.isValid()) {
        qDebug() << "logind sleep inhibitor: error: call returned an invalid file descriptor";
    }

    helper.m_inh = fd;
    qDebug() << "logind sleep inhibitor: success: file descriptor";

    // TO remove:
    //helper.m_inh.reset();
    //qDebug() << "logind sleep inhibitor: removed";

    return a.exec();
}

#include "main.moc"
